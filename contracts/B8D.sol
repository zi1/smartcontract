/**
 *Submitted for verification at BscScan.com on 2022-02-17
*/

pragma solidity 0.5.16;

import "./BEP20Standard.sol";

// ----------------------------------------------------------------------------
// B8D main contract (2022)
//
// Symbol       : B8D
// Name         : B8DEX
// Total supply : 1.000.000.000
// Decimals     : 18
// ----------------------------------------------------------------------------
// SPDX-License-Identifier: MIT
// ----------------------------------------------------------------------------


contract B8D is BEP20Standard {
	constructor() public {
		name = "B8DEX";
		decimals = 18;
		symbol = "B8D";
		version = "1.0";
		_totalSupply = 1000000000 * 10**uint(decimals);
		running = true;

		balances[owner] = _totalSupply;

		emit Transfer(address(0), owner, _totalSupply);
	}
}
