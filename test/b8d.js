let tokenж
const name = 'B8DEX';
const symbol = 'B8D';
const decimals = 18;
const _totalSupply = 1000000000 * 10**decimals;
let owner;
let acc1;
let acc2;
let acc3;
const B8D = artifacts.require("B8D");

contract('B8D', (accounts) => {

  beforeEach(async () => {
    token = await B8D.new()
    owner = accounts[0];
    acc1 = accounts[1];
    acc2 = accounts[2];
    acc3 = accounts[3];
  })

  // BASIC INFORMATION
  it('Basic information: test correct setting of vanity information', async () => {
    const tokenName = await token.name.call()
    assert.strictEqual(tokenName, name, `should be ${name}`)

    const tokenDecimal = await token.decimals.call()
    assert.strictEqual(tokenDecimal.toNumber(), decimals, `should be ${decimals}`)

    const tokenSymbol = await token.symbol.call()
    assert.strictEqual(tokenSymbol, symbol, `should be ${symbol}`)
  })

  it(`Basic information: should put 1000000000000000000000000000 B8D in the first account`, async () => {
    const balance = await token.balanceOf.call(owner);

    assert.equal(balance.valueOf(), _totalSupply, `${_totalSupply} wasn't in the owner account`);
  });

  // START/STOP
  it(`Start/Stop: stop`, async () => {
    const running = await token.startStop.call();

    assert.equal(running, true, `should be stop`);
  });

  it(`Start/Stop: start`, async () => {
    const running = await token.startStop.call();

    assert.equal(running, true, `should be start`);
  });

  // TRANSERS
  it('transfers: should transfer 10000 to acc1 from owner', async () => {
    await token.transfer(acc1, 10000);
    const balance = await token.balanceOf.call(acc1);
    assert.equal(balance.valueOf(), 10000, `10000 wasn't in the acc1 account`);
  })

  it('transfers: should fail when trying to transfer 10001 to acc2 from acc1 having 10000', async () => {
    let threw = false;
    try {
      await token.transferFrom.call(acc1, acc2, 10001);
    } catch (e) {
      threw = true;
    };
    assert.equal(threw, true, `should be true`);
  })

  it('transfers: should handle zero-transfers normally', async () => {
    assert(await token.transferFrom.call(acc1, acc2, 0), 'zero-transfer has failed');
  })

  it('transfers: should complete multi Transfers', async () => {
    const to = [acc1, acc2, acc3];
    const values = [1230000, 2340000, 5670000];
    await token.multiTransfer(to, values);

    const balanceAcc1 = await token.balanceOf.call(acc1);
    const balanceAcc2 = await token.balanceOf.call(acc2);
    const balanceAcc3 = await token.balanceOf.call(acc3);

    assert.equal(balanceAcc1.valueOf(), 1230000, `1230000 wasn't in the acc1 account`);
    assert.equal(balanceAcc2.valueOf(), 2340000, `2340000 wasn't in the acc2 account`);
    assert.equal(balanceAcc3.valueOf(), 5670000, `5670000 wasn't in the acc3 account`);
  })

  // MINT
  it('mint', async () => {
    await token.mint(1000000000);
    const totalSupply = await token.totalSupply.call()

    assert.equal(totalSupply, 1000000000000000001000000000, `1000000000000000001000000000 wasn't in the totalSupply`);
  })

  // BURN
  it('burn', async () => {
    await token.burn(1000000000);
    const totalSupply = await token.totalSupply.call()
    assert.equal(totalSupply, 1000000000000000000000000000, `1000000000000000000000000000 wasn't in the totalSupply`);
  })
});
