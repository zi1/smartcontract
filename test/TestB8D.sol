pragma solidity >=0.4.25 <0.7.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/B8D.sol";

contract TestB8D {

  B8D public _B8D;
  address owner;

  function beforeEach() public {
    _B8D = new B8D();
    owner = _B8D.owner();
  }


  function testInitialBalanceUsingDeployedContract() public {
    uint expected = 1000000000000000000000000000;

    Assert.equal(_B8D.balanceOf(owner), expected, "Owner should have 1000000000000000000000000000 B8D initially");
  }
}
